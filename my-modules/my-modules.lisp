;;; my-modules.lisp

;;; Copyright 2023 Christian Gimenez
;;;

;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.

;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(defpackage #:my-modules
  (:use #:cl :stumpwm)
  (:export #:already-loaded-p
           #:start-all
           #:start-module
           #:can-start-module-p
           #:show-loaded-modules))

(in-package #:my-modules)

(defvar executed-modules nil
  "Modules already loaded and executed.
They are running! If not, you should remove it from here!")

(defun already-loaded-p (module-name)
  "Is the module MODULE-NAME already loaded?"
  (not (not (member module-name executed-modules))))

(defun mark-loaded (module-name)
  "Mark the MODULE-NAME module as loaded.
If it is already marked, do nothing."
  (unless (already-loaded-p module-name)
    (push module-name executed-modules)))

(defvar function-alist '((ttf-fonts . (run-module-ttf-fonts
                                       t))
                         (screenshot . (run-module-screenshot
                                        t))
                         (clipboard-history . (run-module-clipboard-history
                                               t))
                         (stumptray . (run-module-stumptray
                                       t))
                         ;;my-stumpwm-module-stump-nm
                         (pamixer . (run-module-pamixer
                                     can-load-pamixer-p))
                         (stumpwm-sndioctl . (run-module-stumpwm-sndioctl
                                              can-load-stumpwm-sndioctl-p))
                         (cpu . (run-module-cpu can-load-cpu-p)))
  "Available stumpwm functions to start and check modules.
An alist with the module name and a list of two functions: the run
and the check function.  In other words, each element is:
(MODULE-NAME . (RUN-FUNCTION CHECK-FUNCTION)).

If the check function is t, then no checks are performed and the
module is always executed.")

(defun get-check-function (module-name)
  "Return the check function from `function-alist'.
Return nil if MODULE-NAME is not present."
  (cadr (cdr (assoc module-name function-alist))))

(defun get-run-function (module-name)
  "Return the run function from `function-alist'.
Return nil if MODULE-NAME is not present."
  (cadr (assoc module-name function-alist)))

(defun start-module (module-name)
  "Run a particular module named MODULE-NAME.
Check, load, configure and mark it as loaded.  If the check is wrong
do nothing and report it as not loaded.

If the module was not found, return 'not-found symbol."
  (let ((module-function (get-run-function module-name)))
    (if module-function
        (funcall module-function)
        'not-found)))

(defun start-all ()
  "Start all module from `functions-list'."
  (mapcar (lambda (tuple)
            (funcall (car (cdr tuple))))
          function-alist))

(defun can-start-module-p (module-name)
  "Return T if the module can be started.
Do checks to test if the module can be started.  These checks are
associated to each module dependencies."
  (let ((check-function (get-check-function module-name)))
    (if (not check-function)
        nil
        (if (typep check-function 'boolean)
            check-function
            (funcall check-function)))))

(defun loaded-modules-to-string ()
  (format nil "~{~a~^~}"
          (mapcar (lambda (module-name)
                    (format nil "~a ~a~1%"
                            (if (already-loaded-p module-name)
                                "^(:bg \"#a3be8c\" :fg \"#e5e9f0\")+"
                                "^(:bg nil :fg nil)-")
                            module-name))
                  (mapcar 'car function-alist))))

(defcommand start-one-module () ()
  "Start a specific module by name.
Ask the user which module wants to start.  Then, start it, which
involves: check, load, configure, and mark it as loaded."
  (start-module
   (read-from-string
    (car (select-from-menu (current-screen)
                           (mapcar (lambda (tuple)
                                     (symbol-name (car tuple)))
                                   function-alist)
                           "Which module?")))))
  
(defcommand show-started-modules () ()
  "Show all modules, which were loaded and which not." 
  (message (format nil "Modules (with \"+\" when loaded): ~1%~a"
                   (loaded-modules-to-string))))

(defcommand start-all-modules () ()
  "Start all modules and show which one were loaded.
Check, load, configure, and mark it as loaded.  If the check went
wrong, do nothig."
  (my-modules:start-all)
  (show-started-modules))

;; --------------------------------------------------
;; For module checking when a program is needed.

(defun executables ()
  "Find all executables.

Copied from @ryukinix. https://gist.github.com/ryukinix/5273af4b25dec53ed9f078bd7e350d88"
  (loop with path = (uiop:getenv "PATH")
        for p in (uiop:split-string path :separator ":")
        for dir = (probe-file p)
        when (uiop:directory-exists-p dir)
          append (uiop:directory-files dir)))

(defun find-executable (name)
  "Find the executable named NAME.

Copied from @ryukinix.
https://gist.github.com/ryukinix/5273af4b25dec53ed9f078bd7e350d88
"
  (find name (executables)
        :test #'equalp
        :key #'pathname-name))

(defmacro funcallpack (pack symbol &rest args)
  "Call a function from a package that may not be loaded.
Using this macro avoids SBCL warnings and errors because the function
is used but it does not exists or were imported yet."
  `(funcall (find-symbol ,(symbol-name symbol) ,pack) ,@args))

;; --------------------------------------------------
;; --------------------------------------------------
;; ttf-fonts module

(defun download-clx-truetype ()
  "Clone clx-truetype repository if it does not exists.
The clx-truetype is not part of Quicklisp, it must be downloaded as a
local project. "
  (unless (probe-file "~/quicklisp/local-projects/clx-truetype")
    (message "clx-truetype module is not in quicklisp... Cloning repository!")
    (run-shell-command "git clone https://github.com/l04m33/clx-truetype.git ~/quicklisp/local-projects/clx-truetype" t)))

(defun run-module-ttf-fonts ()
  "Load the ttf-fonts module."
  (unless (already-loaded-p 'ttf-fonts)

    ;; checks...
    (download-clx-truetype)
    ;; load...
    (ql:quickload :clx-truetype)
       
    (load-module "ttf-fonts")
    ;; configure...
    ;; Add another fontdir
    (unless (member "~/.local/share/fonts" (eval (find-symbol "*FONT-DIRS*" :xft))
                    :test 'string-equal)
      (set (find-symbol "*FONT-DIRS*" :xft) (append '("~/.local/share/fonts")
                                                    (eval (find-symbol "*FONT-DIRS*" :xft)))))
    
    (funcallpack :xft cache-fonts)

    ;; (set-font (make-instance 'xft:font :family "sachacHand" :subfamily "Regular" :size 16))
    ;; (set-font (make-instance 'xft:font :family "Hack" :subfamily "Regular" :size 9))
    (set-font (make-instance (find-symbol "FONT" :xft)
                             :family "Hack" :subfamily "Regular"
                             :size 9))
    
    (mark-loaded 'ttf-fonts)))
  

;; --------------------------------------------------
;; screenshot module

(defun run-module-screenshot ()
  "Load the screenshot module."
  (unless (already-loaded-p 'screenshot)

    ;; no checks!
    ;; load
    (ql:quickload :zpng)
    (load-module "screenshot")
    ;; no config!

    (mark-loaded 'screenshot)))

;; --------------------------------------------------
;; clipboard-history module
  
(defun run-module-clipboard-history ()
  "Install and load the module clipboard-history."
  (unless (already-loaded-p 'clipboard-history)

    ;; no checks!
    ;; load
    (load-module "clipboard-history")
    ;; (funcall (find-symbol "START-CLIPBOARD-MANAGER" :clipboard-history))
    (funcallpack :clipboard-history start-clipboard-manager)
    
    ;; config!
    (define-key *root-map* (kbd "C-y") "show-clipboard-history")
    
    (mark-loaded 'clipboard-history)))

;; --------------------------------------------------
;; pamixer module

(defun can-load-pamixer-p ()
  "The module pamixer can be loaded?"
  (and (uiop:directory-exists-p "~/.stumpwm.d/modules/pamixer")
       (equal :LINUX (uiop:operating-system))))

(defun run-module-pamixer ()
  "Install and load the pamixer module."
  ;; (stumpwm:add-to-load-path "~/.stumpwm.d/modules/pamixer")
  (when (and (not (already-loaded-p 'pamixer))
             ;; checks...
             (can-load-pamixer-p))

    ;; load...
    (load-module "pamixer")

    ;; config...
    (define-key *top-map* (kbd "XF86AudioRaiseVolume") "pamixer-volume-up")
    (define-key *top-map* (kbd "XF86AudioLowerVolume") "pamixer-volume-down")
    (define-key *top-map* (kbd "XF86AudioMute") "pamixer-toggle-mute")

    (mark-loaded 'pamixer)))

;; --------------------------------------------------
;; stumptray module

(defun run-module-stumptray ()
  "Install and load the xembed module."
  (unless (already-loaded-p 'stumptray)

    ;; no checks!
    ;; load
    (ql:quickload "xembed")
    (stumpwm:load-module "stumptray")    
    ;; (funcall (find-symbol "STUMPTRAY" :stumptray))
    (funcallpack :stumptray stumptray)
    ;; config...
    (setq *screen-mode-line-format*
          (append  *screen-mode-line-format* '("    ")))
    
    (mark-loaded 'stumptray)))


;; --------------------------------------------------
;; stumpwm-sndioctl module

(defun can-load-stumpwm-sndioctl-p ()
  "Can the stumpwm-sndioctl module be loaded?"
  (and (equal :BSD (uiop:operating-system))
       (find-executable "sndioctl")))

(defun run-module-stumpwm-sndioctl ()
  "Install and load the xembed module."
  (when (and (not (already-loaded-p 'stumpwm-sndioctl))
             ;; checks...
             (can-load-stumpwm-sndioctl-p))
    
    ;; load...
    (stumpwm:load-module "stumpwm-sndioctl")    

    ;; config...
    (define-key *top-map* (kbd "XF86AudioMute") "toggle-mute")
    (define-key *top-map* (kbd "XF86AudioLowerVolume") "volume-down")
    (define-key *top-map* (kbd "XF86AudioRaiseVolume") "volume-up")
    
    (mark-loaded 'stumpwm-sndioctl)))

;; --------------------------------------------------
;; cpu module

(defun can-load-cpu-p ()
  "Can the cpu module be loaded?"
  (equal :LINUX (uiop:operating-system)))

(defun run-module-cpu ()
  "Install and load the cpu module."
  (when (and (not (already-loaded-p 'cpu))
             ;; checks
             (can-load-cpu-p))

    ;; load
    (stumpwm:load-module "cpu")
    (ql:quickload :cl-cpus)
    
    ;; config
    (set (find-symbol "*CPU-MODELINE-FMT*" :cpu) "%c")
    (push "%C | " stumpwm:*screen-mode-line-format*)
    (funcallpack :cpu "SET-CPU-NUMBER"
                 (funcallpack :cl-cpus "GET-NUMBER-OF-PROCESSORS"))

    (mark-loaded 'cpu)))
