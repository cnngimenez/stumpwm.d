#! /usr/bin/env fish

xrandr --output eDP --right-of HDMI-A-0

picom &

feh --bg-max --randomize ~/.stumpwm.d/wallpapers &

setxkbmap es dvorak

xautolock -time 5 -locker 'i3lock -c 000000' &

xsetroot -solid "#2e3440"

netxcloud &
syncthing &

kdeconnect-indicator &
