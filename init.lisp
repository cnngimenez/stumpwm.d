;; [[file:Readme.org::*In SBCL/StumpWM: Load slynk][In SBCL/StumpWM: Load slynk:1]]
(ql:quickload :slynk)
;; In SBCL/StumpWM: Load slynk:1 ends here

;; [[file:Readme.org::*start-slynk][start-slynk:1]]
(stumpwm:defcommand start-slynk () ()
    "Start Slynk server to let Emacs and Sly to connect to this SBCL instance."
    (slynk:create-server :dont-close t)
    (message "Slynk started. Run M-x sly-connect in Emacs."))
;; start-slynk:1 ends here

;; [[file:Readme.org::*Use stumpwm namespace][Use stumpwm namespace:1]]
(in-package :stumpwm)
;; Use stumpwm namespace:1 ends here

;; [[file:Readme.org::*Load quicklisp][Load quicklisp:1]]
(load "~/quicklisp/setup.lisp")
;; Load quicklisp:1 ends here

;; [[file:Readme.org::*Variable with nord colours][Variable with nord colours:1]]
(defconstant nord-colours
  '(;; Polar Night
    "#2e3440"
    "#3b4252"
    "#434c5e"
    "#4c566a"
    ;; Snow Storm
    "#d8dee9"
    "#e5e9f0"
    "#eceff4"
    ;; Frost
    "#8fbcbb"
    "#88c0d0"
    "#81a1c1"
    "#5e81ac"
    ;; Aurora
    "#bf616a"
    "#d08770"
    "#ebcb8b"
    "#a3be8c"
    "#b48ead")
  "Nord colour list, from nord0 to nord15.")
;; Variable with nord colours:1 ends here

;; [[file:Readme.org::*List nord colours][List nord colours:1]]
(defcommand list-nord-colours () ()
  (let ((*suppress-echo-timeout* t))
    (message "Nord colours:~1% ~{~A~^~1% ~}~1% Press C-t b to banish."
             (map 'list (lambda (str index)
                          (format nil "^(:bg ~S)~d: ~S" str index str))
                  nord-colours
                  (alexandria:iota (length nord-colours))))))
;; List nord colours:1 ends here

;; [[file:Readme.org::*Chage prefix C-t -> S-t][Chage prefix C-t -> S-t:1]]
(set-prefix-key (kbd "s-t"))
;; Chage prefix C-t -> S-t:1 ends here

;; [[file:Readme.org::*yes-no-diag][yes-no-diag:1]]
(defun yes-no-diag (message)
  "Ask the user a question and let them choose for yes or no."
  (string-equal "yes" (car
                       (select-from-menu (current-screen) '("Yes" "No") message))))
;; yes-no-diag:1 ends here

;; [[file:Readme.org::*Input and message possition][Input and message possition:1]]
(setf *message-window-gravity :top)
(setf *message-window-input-gravity* :top)
(setf *input-window-gravity* :top)
;; Input and message possition:1 ends here

;; [[file:Readme.org::*Setting bar colour][Setting bar colour:1]]
(set-fg-color (nth 6 nord-colours))
(set-bg-color (nth 1 nord-colours))
(set-border-color (nth 4 nord-colours))
(set-msg-border-width 2)
;; Setting bar colour:1 ends here

;; [[file:Readme.org::*Modeline position and colour][Modeline position and colour:1]]
(setf *mode-line-position* :top)
(setf *mode-line-border-width* 1)
(setf *mode-line-pad-x* 5)
(setf *mode-line-pad-y* 5)
(setf *mode-line-background-color* (nth 0 nord-colours))
(setf *mode-line-foreground-color* (nth 4 nord-colours))
(setf *mode-line-border-color* (nth 4 nord-colours))
;; Modeline position and colour:1 ends here

;; [[file:Readme.org::*Modeline format][Modeline format:1]]
(setf *window-format* "%m%n%s%c")
(setf *screen-mode-line-format* (list "[^B%n^b] %W^>%d"))

(setf *time-modeline-string* "%a %b %e %k:%M")
;; Modeline format:1 ends here

;; [[file:Readme.org::*Timeout][Timeout:1]]
(setf *mode-line-timeout* 2)
;; Timeout:1 ends here

;; [[file:Readme.org::*Mode line on][Mode line on:1]]
;; (enable-mode-line (current-screen) (current-head) t)
(mapc (lambda (x) (enable-mode-line (current-screen) x t))
              (screen-heads (current-screen)))
;; Mode line on:1 ends here

;; [[file:Readme.org::*Screenlock][Screenlock:1]]
(defcommand screenlock () ()
  (run-shell-command "i3lock -c 000000"))
;; Screenlock:1 ends here

;; [[file:Readme.org::*alist-define-keys][alist-define-keys:1]]
(defun alist-define-keys (map alist)
  "define key using alist."
  (loop for (key . command) in alist
        do (define-key map (kbd key) command)))
;; alist-define-keys:1 ends here

;; [[file:Readme.org::*Defining some keys][Defining some keys:1]]
(alist-define-keys *root-map* '(("C-l" . "screenlock")))
;; Defining some keys:1 ends here

;; [[file:Readme.org::*Suspend computer][Suspend computer:1]]
(defcommand suspend-computer () ()
  "Suspend the computer"
  (when (yes-no-diag "Suspend?")
    (message "^(:bg ~S% :fg ~S%)Suspending..."
             (nth 11 nord-colours) (nth 2 nord-colours))
    (screenlock)
    (run-shell-command "systemctl suspend")))
;; Suspend computer:1 ends here

;; [[file:Readme.org::*Set module path][Set module path:1]]
(setf *module-dir* "~/.stumpwm.d/modules/")
;; Set module path:1 ends here

;; [[file:Readme.org::*Clone if module path does not exists][Clone if module path does not exists:1]]
(defun my-download-module-dir-maybe ()
  "Download the StumpWM module dir if it does not exists."
  (unless (probe-file *module-dir*)

    (message "^(:bg ~S)No module dir founded... cloning repository!~1%This may take a while..."
             (nth 12 nord-colours))

    (let ((cmd (format nil "git clone http://github.com/stumpwm/stumpwm-contrib.git ~a" *module-dir*)))
      (print (format nil "Executing command: ~s" cmd))
      (run-shell-command cmd t))))

(my-download-module-dir-maybe)
;; Clone if module path does not exists:1 ends here

;; [[file:Readme.org::*Initialise module path][Initialise module path:1]]
(init-load-path *module-dir*)
;; Initialise module path:1 ends here

;; [[file:Readme.org::*Load my-module library][Load my-module library:1]]
(load "~/.stumpwm.d/my-modules/my-modules.lisp")
(my-modules:start-all)
;; Load my-module library:1 ends here

;; [[file:Readme.org::*Startup groups][Startup groups:1]]
(gnew "Emacs")
(gnew "Docs")
;; Startup groups:1 ends here

;; [[file:Readme.org::*Startup programs][Startup programs:1]]
(run-shell-command "fish ~/.stumpwm.d/stumpwm-startup.fish")
;; Startup programs:1 ends here

;; [[file:Readme.org::*Say hello!][Say hello!:1]]
(mesage "StumpWM Initialised...\n^B^2Welcome!")
;; Say hello!:1 ends here
